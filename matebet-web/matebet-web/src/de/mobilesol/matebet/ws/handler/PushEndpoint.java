package de.mobilesol.matebet.ws.handler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import de.mobilesol.matebet.web.dto.BetDTO;
import de.mobilesol.matebet.web.dto.BetDTO.BetStatus;
import de.mobilesol.matebet.web.dto.PushNotificationDTO;
import de.mobilesol.matebet.web.dto.PushNotificationDTO.Page;
import de.mobilesol.matebet.web.dto.PushRecipientDTO;
import de.mobilesol.matebet.web.dto.UserDTO;
import de.mobilesol.matebet.web.exception.ResourceNotFoundException;
import de.mobilesol.matebet.web.service.ClosedBetService;
import de.mobilesol.matebet.web.service.PushHistoryService;
import de.mobilesol.matebet.web.service.PushService;
import de.mobilesol.matebet.web.service.UserService;

public class PushEndpoint {

	private static final long serialVersionUID = 1L;

	private PushService pushService = new PushService();
	private PushHistoryService pushHistoryService = new PushHistoryService();
	private UserService userService = new UserService();

	private static final Logger log = Logger
			.getLogger(PushEndpoint.class.getName());

	private static int cnt = 0;

	public Object resendPushTest(int userId, String key,
			HttpServletResponse resp) {
		PushNotificationDTO push = pushHistoryService.getPushNotification(key);
		if (push == null) {
			new ResourceNotFoundException(0, "the key is invalid");
		}

		return pushService.send(userId, push);
	}

	public Object sendTest(String user, HttpServletResponse resp) {

		List<Object> ret = new ArrayList<Object>();
		List<UserDTO> users = userService.getAllUsers(user);
		for (UserDTO u : users) {

			log.info("send push to user=" + u);
			Map<String, Object> ob = new LinkedHashMap<>();
			ret.add(ob);

			List<BetDTO> betsTmp = new ClosedBetService()
					.getClosedBets(u.userId);
			List<BetDTO> bets = new ArrayList<BetDTO>();
			for (BetDTO b : betsTmp) {
				if (b.status.equals(BetStatus.FINISHED)) {
					b.groups = null;
					b.leagueId = 0;
					b.matchTips = null;
					b.lastMatchdate = null;
					b.confirmedBy = null;
					b.archivedBy = null;
					b.resultByMatchByUser = null;
					b.resultTips = null;
					b.created = null;
					b.leagueName = null;
					b.results = null;
					bets.add(b);
				}
			}

			BetDTO bet;
			Page page;
			if (bets.isEmpty()) {
				bet = new BetDTO();
				bet.betId = 0;
				page = null;
			} else {
				bet = bets.get(new Random().nextInt(bets.size()));
				page = Page.BET_STATUS;
			}

			log.info("send bet = " + bet);
			if (bets.size() > 5) {
				bets = bets.subList(0, new Random().nextInt(4) + 1);
			}

			Map<String, Object> payload = new HashMap<String, Object>();
			payload.put("bets", bets);
			payload.put("param1", "test2");
			payload.put("param2", "test3");

			PushNotificationDTO push = new PushNotificationDTO(
					"Hi " + u.name + ", this is a gcm message #" + bets.size(),
					"there is a test message #" + bets.size() + " for " + u,
					page, payload);

			Object o = pushService.send(u.userId, push);

			ob.put(u.name + "/" + u.userId, (o != null ? o : "-"));
		}
		cnt++;

		return ret;
	}

	public Object registerPush(PushRecipientDTO push, HttpServletResponse resp)
			throws IOException {
		return pushService.registerPush(push);
	}

	public Object unregisterPush(int user, String uuid,
			HttpServletResponse resp) throws IOException {
		log.info("user=" + user + ";" + uuid);
		return pushService.unregisterPush(user, uuid);
	}

	public Object sendPush(PushNotificationDTO push, HttpServletResponse resp)
			throws IOException {

		List<Object> ob = new ArrayList<Object>();
		List<UserDTO> users = userService.getAllUsers(null);

		for (UserDTO u : users) {

			log.info("send push to user=" + u);
			ob.add(u.name);

			Object o = pushService.send(u.userId, new PushNotificationDTO(
					push.title, push.message, push.page, push.payload));
			ob.add(o);
		}

		return ob;
	}

	public Object sendReport(String status, HttpServletResponse resp)
			throws IOException {
		log.info("***" + status);
		return null;
	}
}
