package de.mobilesol.matebet.ws.handler;

import java.io.IOException;
import java.io.Reader;

import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import de.mobilesol.matebet.web.dto.MatchTipDTO;
import de.mobilesol.matebet.web.dto.MatchTipDTO.TipsByMatchMap;
import de.mobilesol.matebet.web.exception.MatebetValidationException;
import de.mobilesol.matebet.web.service.MatchTipService;

public class MatchTipEndpoint {

	private static final long serialVersionUID = 1L;

	private MatchTipService matchService = new MatchTipService();

	public MatchTipDTO handleGetMatch(int betId, int userId,
			HttpServletResponse resp) throws IOException {
		MatchTipDTO u = matchService.getMatchTips(betId, userId);
		return u;
	}

	public TipsByMatchMap handleAddMatchTip(int betId, int groupId, int userId,
			Reader in, HttpServletResponse resp) throws IOException {

		Gson gson = new Gson();
		TipsByMatchMap u = gson.fromJson(in, TipsByMatchMap.class);
		if (u == null) {
			throw new MatebetValidationException(307, "Illegal tip request");
		}
		matchService.addMatchTips(betId, groupId, userId, u);
		return u;
	}
}
