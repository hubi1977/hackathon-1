package de.mobilesol.matebet.ws.handler;

import java.io.IOException;
import java.io.Reader;

import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import de.mobilesol.matebet.web.dto.BetDTO;
import de.mobilesol.matebet.web.exception.ResourceNotFoundException;
import de.mobilesol.matebet.web.service.master.BetService;

public class BetEndpoint {

	private static final long serialVersionUID = 1L;

	private BetService betService = new BetService();

	public BetDTO handleGetBet(int betId, HttpServletResponse resp)
			throws IOException {
		BetDTO u = betService.getBet(betId);
		if (u == null) {
			throw new ResourceNotFoundException(100,
					"Bet with id " + betId + " not found");
		}

		return u;
	}

	public int handleAdd(Reader in, HttpServletResponse resp)
			throws IOException {

		Gson gson = new Gson();
		BetDTO u = gson.fromJson(in, BetDTO.class);
		int id = betService.addBet(u);
		return id;
	}

	public int handleAddFu(BetDTO u, HttpServletResponse resp)
			throws IOException {
		return betService.addBet(u);
	}

	public int handleAddPhil(BetDTO u, HttpServletResponse resp)
			throws IOException {

		return betService.addBet(u);
	}

	public BetDTO handleConfirmBet(int betId, int opponentId,
			HttpServletResponse resp) throws IOException {

		BetDTO b = betService.confirmBet(betId, opponentId);
		return b;
	}

	public BetDTO handleDeclineBet(int betId, int opponentId,
			HttpServletResponse resp) throws IOException {

		BetDTO b = betService.declineBet(betId, opponentId);
		return b;
	}
}
