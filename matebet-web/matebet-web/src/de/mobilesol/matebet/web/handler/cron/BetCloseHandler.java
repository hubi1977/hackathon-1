package de.mobilesol.matebet.web.handler.cron;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import de.mobilesol.matebet.web.dto.BetDTO;
import de.mobilesol.matebet.web.dto.MatchTipDTO;
import de.mobilesol.matebet.web.service.MatchTipService;
import de.mobilesol.matebet.web.service.master.BetService;

public class BetCloseHandler extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private static final Logger log = Logger
			.getLogger(BetCloseHandler.class.getName());

	private Gson gson = new GsonBuilder().setPrettyPrinting()
			.setDateFormat("yyyy-MM-dd'T'HH:mm:ss.sssZ").create();

	private BetService betService = new BetService();
	private MatchTipService matchTipService = new MatchTipService();

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		log.info("going to close bets");
	}
}
