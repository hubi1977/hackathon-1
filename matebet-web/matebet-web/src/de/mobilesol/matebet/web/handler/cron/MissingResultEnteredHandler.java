package de.mobilesol.matebet.web.handler.cron;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import de.mobilesol.matebet.web.dto.GroupDTO;
import de.mobilesol.matebet.web.dto.LeagueDTO;
import de.mobilesol.matebet.web.dto.MatchDTO;
import de.mobilesol.matebet.web.service.master.GroupService;
import de.mobilesol.matebet.web.service.master.LeagueService;
import de.mobilesol.matebet.web.service.master.MatchService;
import de.mobilesol.matebet.web.service.master.TeamService;
import de.mobilesol.matebet.web.util.SendMail;

public class MissingResultEnteredHandler extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private static final Logger log = Logger
			.getLogger(MissingResultEnteredHandler.class.getName());

	private MatchService matchService = new MatchService();
	private GroupService groupService = new GroupService();
	private LeagueService leagueService = new LeagueService();
	private TeamService teamService = new TeamService();

	private SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy HH:mm",
			Locale.GERMANY);

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		int nb;
		try {
			nb = Integer.parseInt(req.getParameter("days"));
		} catch (Exception e) {
			nb = 60;
		}

		log.info("missing result entered " + nb);
		List<MatchDTO> pastMatches = matchService.getLatestMatches(nb);
		Collections.sort(pastMatches, new Comparator<MatchDTO>() {

			@Override
			public int compare(MatchDTO o1, MatchDTO o2) {
				int r = o1.leagueId - o2.leagueId;
				if (r != 0) {
					return r;
				}

				long r1 = o1.matchdate.getTime() - o2.matchdate.getTime();
				if (r1 < 0) {
					return -1;
				} else if (r1 > 0) {
					return 1;
				}

				return o1.matchId - o2.matchId;
			}
		});

		Set<Integer> notExistingLeagues = new HashSet<Integer>();
		Map<Integer, LeagueDTO> leagueById = new HashMap<Integer, LeagueDTO>();
		Map<Integer, GroupDTO> groupById = new HashMap<Integer, GroupDTO>();

		StringBuffer strb = new StringBuffer();
		LeagueDTO oldLeague = null;
		GroupDTO oldGroup = null;

		boolean foundAny = false;
		boolean groupOpen = false;
		for (MatchDTO m : pastMatches) {

			if (notExistingLeagues.contains(m.leagueId)) {
				continue;
			}
			foundAny = true;
			LeagueDTO l = leagueById.get(m.leagueId);
			if (l == null) {
				l = leagueService.getLeague(m.leagueId);
				leagueById.put(m.leagueId, l);
			}
			if (l == null) {
				// League may exist not more
				notExistingLeagues.add(m.leagueId);
				log.info("leagues " + notExistingLeagues
						+ " do not exist any more");
				continue;
			}

			GroupDTO g = groupById.get(m.groupId);
			if (g == null) {
				g = groupService.getGroup(m.groupId);
				groupById.put(m.groupId, g);
			}

			if (!l.equals(oldLeague)) {
				if (groupOpen) {
					strb.append("</ul>");
				}
				oldLeague = l;
				strb.append("<h1>League: " + l.title + "</h1>");
			}

			if (!g.equals(oldGroup)) {
				if (groupOpen) {
					strb.append("</ul>");
				}
				oldGroup = g;
				strb.append("<h2>Gruppe: " + g.name + "</h2>");
				strb.append("<ul>");
				groupOpen = true;
			}

			strb.append("<li>" + format.format(m.matchdate) + ": "
					+ teamService.getTeam(m.team1.teamId).name + ": "
					+ teamService.getTeam(m.team2.teamId).name + "</li>");
		}
		if (groupOpen) {
			strb.append("</ul>");
		}
		resp.getWriter().write(strb.toString());

		if (!foundAny) {
			new SendMail().send("mateBet Ergebnisse",
					"<p>Hallo zusammen,</p><p>Es sind alle Spielergebnisse der letzten "
							+ nb
							+ " Tage eingetragen</p><p>Viele Grüße,<br/><br/>Lui</p>",
					"Info", "info@matebet.net", null);
		} else {
			new SendMail().send("mateBet Ergebnisse",
					"<p>Hallo zusammen,</p><p>folgende Spielergebnisse der letzten "
							+ nb + " Tage sind noch nicht bekannt:</p>" + strb
							+ "<p>Viele Grüße,<br/><br/>Lui</p>" + "<br/><br/>"
							+ "<p>Ergebnisse eintragen: <a href=\"http://www.openligadb.de/\">http://www.openligadb.de/</a></p>",
					"Info", "info@matebet.net", null);
		}
	}
}
