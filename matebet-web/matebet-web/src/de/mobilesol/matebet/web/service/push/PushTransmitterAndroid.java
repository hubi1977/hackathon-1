package de.mobilesol.matebet.web.service.push;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.google.appengine.api.utils.SystemProperty;
import com.google.gson.Gson;

import de.mobilesol.matebet.web.dao.PushRecipientDAO;
import de.mobilesol.matebet.web.dto.PushRecipientDTO;
import de.mobilesol.matebet.web.dto.PushNotificationDTO;
import de.mobilesol.matebet.ws.dto.ErrorDTO;

public class PushTransmitterAndroid extends AbstractPushTransmitter {

	private static final Logger log = Logger
			.getLogger(PushTransmitterAndroid.class.getName());

	private PushRecipientDAO dao = new PushRecipientDAO();
	int cnt = 0;

	public PushTransmitterAndroid() {
		super.platform = "Android";
	}

	@Override
	protected List<TransmitterResult> sendPushInternal(
			List<PushRecipientDTO> list, PushNotificationDTO push) {

		if (list.isEmpty()) {
			return new ArrayList<>();
		}

		log.info("sending to android");
		List<TransmitterResult> result = new ArrayList<TransmitterResult>();

		HttpURLConnection connection = null;
		try {
			URL url = new URL("https://gcm-http.googleapis.com/gcm/send");
			connection = (HttpURLConnection) url.openConnection();
		} catch (Exception e) {
			TransmitterResult tr = new TransmitterResult();
			result.add(tr);
			tr.userId = list.get(0).userId;
			tr.token = list.get(0).token;
			tr.text = e.getMessage();

			return result;
		}

		try {

			String applicationId = SystemProperty.applicationId.get();
			String auth;
			if (applicationId.equals("luitest123")) {
				// test
				auth = "AIzaSyALlXVQfhqLvpkK1vxEOIy8iSyzy4JTfCQ";
			} else if (applicationId.equals("matebet-1222")) {
				// prod
				auth = "AIzaSyCKkOCgCnT4Nik_o5ZMixaa1Kbx5LZ0-AY";
			} else {
				throw new IllegalStateException(
						"Illegal appID: " + applicationId);
			}

			// prod
			// AIzaSyBWKr7dpkZP02CP2Vv7rtI5qsEAlZtcdfk

			// test
			// AIzaSyALlXVQfhqLvpkK1vxEOIy8iSyzy4JTfCQ
			connection.setRequestProperty("Authorization", "key=" + auth);

			log.info("sending push with auth " + auth);
			AndroidPN pn = new AndroidPN();
			if (list.size() > 1) {
				pn.registration_ids = new ArrayList<String>();
				for (PushRecipientDTO p : list) {
					pn.registration_ids.add(p.token);
				}
			} else {
				pn.to = list.get(0).token;
			}

			Map<String, Object> payload = new HashMap<String, Object>();
			payload.put("platform", "android");

			if (push.payload != null) {
				payload.putAll(push.payload);
			}
			if (push.page != null) {
				payload.put("page", push.page.value);
			}
			pn.data.put("title", push.title);
			pn.data.put("notId", cnt);
			pn.data.put("message", push.message);

			pn.data.putAll(payload);
			pn.collapse_key = "xx" + (cnt++ % 3);

			Object ret = read(pn, connection);

			for (PushRecipientDTO p : list) {
				TransmitterResult tr = new TransmitterResult();
				result.add(tr);
				tr.userId = p.userId;
				tr.token = p.token;
				tr.text = ret;
			}

			if (!(ret instanceof ErrorDTO)) {
				AndroidResult res = new Gson().fromJson(ret.toString(),
						AndroidResult.class);
				log.info("got android result1" + ret);
				log.info("got android result2" + res);
				if (res.results == null
						|| res.failure + res.success != res.results.size()
						|| res.results.size() != list.size()) {
					log.warn("serious inconsistency" + res.results + ";" + list
							+ ";" + ret);
				} else {
					log.info("got android result: " + res.results.size());
					for (int i = 0; i < res.results.size(); i++) {
						if (res.results.get(i).registration_id != null) {
							log.warn(
									"you should use this registration id in future:"
											+ res.results.get(i).registration_id
											+ "; token: " + list.get(i).token);
						}
						if ("NotRegistered".equals(res.results.get(i).error)) {
							log.warn("remove token " + list.get(i).token);
							dao.removePushByToken(list.get(i).userId,
									list.get(i).token);
						} else {
							for (TransmitterResult tr : result) {
								if (tr.token.equals(list.get(i).token)) {
									tr.ok = true;
								}
							}
						}
					}
				}
			}

			return result;
		} catch (IOException e) {
			log.warn("ioexc=", e);
			TransmitterResult tr = new TransmitterResult();
			result.add(tr);
			tr.userId = list.get(0).userId;
			tr.token = list.get(0).token;
			tr.text = e.getMessage();
		} finally {
			connection.disconnect();
		}

		return result;
	}

	private Object read(Object pn, HttpURLConnection connection)
			throws UnsupportedEncodingException, IOException {
		connection.setInstanceFollowRedirects(true);
		connection.setRequestMethod("POST");
		connection.setConnectTimeout(25000);
		connection.setReadTimeout(10000);
		connection.setDoOutput(true);
		connection.setDoInput(true);

		connection.setRequestProperty("Content-Type", "application/json");
		connection.setRequestProperty("User-Agent",
				"Mozilla/5.0 (Windows; U; Windows NT 6.0; ru; rv:1.9.0.11) Gecko/2009060215 Firefox/3.0.11 (.NET CLR 3.5.30729 Lui)");

		Gson gson = new Gson();
		String json = gson.toJson(pn);

		log.info("send " + json);
		connection.getOutputStream().write(json.getBytes("UTF-8"));
		connection.getOutputStream().close();

		StringBuffer strb1 = new StringBuffer();

		byte[] buffer = new byte[2048];
		try {
			InputStream in = connection.getInputStream();
			int l = 0;
			while ((l = in.read(buffer)) > 0) {
				strb1.append(new String(buffer, 0, l));
			}
			in.close();
		} catch (IOException e) {
			// e.printStackTrace();
		}
		if (strb1.length() > 0) {
			log.info("input response: " + strb1);
		}

		StringBuffer strb2 = new StringBuffer();
		InputStream err = connection.getErrorStream();
		if (err != null) {
			int l = 0;
			while ((l = err.read(buffer)) > 0) {
				strb2.append(new String(buffer, 0, l));
			}
			err.close();
		}
		if (strb2.length() > 0) {
			log.info("error response: " + strb2);
		}

		int code = connection.getResponseCode();
		log.info("code=" + code);
		if (code / 100 != 2) {
			ErrorDTO error = gson.fromJson(strb2.toString(), ErrorDTO.class);
			error.code = code;
			log.warn("failed with: " + strb1);
			log.warn("failed with: " + strb2);
			return error;
		}

		return strb1.toString();
	}

	public static class AndroidPN {
		public String to;
		public List<String> registration_ids;
		public PN2 notification;
		public String collapse_key;
		public Map<String, Object> data = new HashMap<String, Object>();

		public static class PN2 {
			public String title, message;
		}
	}

	public static class AndroidResult implements Serializable {
		private static final long serialVersionUID = 1L;

		public String multicast_id;
		public int success, failure, canonical_ids;
		public List<AndroidResultItem> results;

		@Override
		public String toString() {
			return "AndroidResult [multicast_id=" + multicast_id + ", success="
					+ success + ", failure=" + failure + ", canonical_ids="
					+ canonical_ids + ", results=" + results + "]";
		}
	}

	public static class AndroidResultItem implements Serializable {
		private static final long serialVersionUID = 1L;
		public String error, message_id, registration_id;

		@Override
		public String toString() {
			return "AndroidResultItem [error=" + error + ", message_id="
					+ message_id + ", registration_id=" + registration_id + "]";
		}
	}
}
