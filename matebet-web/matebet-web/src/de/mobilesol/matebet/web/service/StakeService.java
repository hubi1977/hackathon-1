package de.mobilesol.matebet.web.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import de.mobilesol.matebet.web.dto.StakeDTO;
import de.mobilesol.matebet.web.dto.UserDTO;

public class StakeService {
	private static final Logger log = Logger
			.getLogger(StakeService.class.getName());

	private static final String[] stakesLess16 = new String[]{"1 Euro",
			"1 Beer"};

	private UserService userService = new UserService();

	public List<StakeDTO> getStakes(List<Integer> userIds) {
		log.info("getStakes(" + userIds + ")");


		List<StakeDTO> stakeList = new ArrayList<StakeDTO>();
		for (String s : stakesLess16) {
			StakeDTO sd = new StakeDTO();
			sd.stake = s;
			stakeList.add(sd);
		}

		return stakeList;
	}
}
