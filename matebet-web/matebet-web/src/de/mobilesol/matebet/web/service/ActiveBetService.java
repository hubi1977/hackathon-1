package de.mobilesol.matebet.web.service;

import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;

import de.mobilesol.matebet.web.dto.ActiveBetStatusDTO;
import de.mobilesol.matebet.web.dto.BetDTO;
import de.mobilesol.matebet.web.dto.BetDTO.BetStatus;
import de.mobilesol.matebet.web.dto.MatchTipDTO;
import de.mobilesol.matebet.web.service.master.BetService;
import de.mobilesol.matebet.web.service.master.MatchService;
import de.mobilesol.matebet.web.util.CalculatePointsUtil;
import de.mobilesol.matebet.web.util.Util;

public class ActiveBetService {

	private static final Logger log = Logger
			.getLogger(ActiveBetService.class.getName());

	private BetService betService = new BetService();
	private MatchService matchService = new MatchService();
	private MatchTipService matchTipService = new MatchTipService();

	public List<BetDTO> getOpenBets(int userId) {
		log.info("getOpenBets(" + userId + ")");

		List<BetDTO> l = betService.getOpenBets(userId);
		log.info("found bets: #=" + l.size() + "; " + l);
		addOpenBetData(l, userId);
		return l;
	}

	public int[] countStats() {
		log.info("countStats");

		List<BetDTO> l = betService.getOpenBets(1);
		log.info("found bets: #=" + l.size() + "; " + l);
		int falling=0;
		int rising= 0;
		for (BetDTO b : l) {
			if (b.title.contains("steigt") || b.title.contains("rising")) {
				rising++;
			} else {
				falling++;
			}
		}
		return new int[]{falling, rising};
	}

	public BetDTO getOpenBet(int betId, int userId) {
		log.info("getOpenBet(" + betId + ", " + userId + ")");

		BetDTO bet = betService.getBet(betId);
		bet.resultTips = matchService.getMatchTips(bet.leagueId);

		addOpenBetData(Collections.singletonList(bet), userId);
		Util.addTeamUrls(bet);

		log.info("activeStatus=" + bet.activeStatus);

		log.info("matchTips=" + bet.matchTips);

		return bet;
	}

	private void addOpenBetData(List<BetDTO> l, int userId) {

		for (BetDTO bet : l) {
			log.info("handle bet " + bet);
			MatchTipDTO ut = null;

			// for running bets the match tips have to be calculated
			if (bet.status == BetStatus.CLOSED) {
				ut = bet.matchTips;
			} else {
				ut = matchTipService.getMatchTips(bet.betId, -1);
				bet.matchTips = ut;
			}

			ActiveBetStatusDTO status = Util.determineActiveStatus(bet, ut,
					userId);
			bet.activeStatus = status;

			// calculate points
			log.info("calculate resultmap with " + bet.resultTips);
			CalculatePointsUtil calc = new CalculatePointsUtil();
			bet.resultByMatchByUser = calc.calculatePoints(bet, ut,
					bet.resultTips);

			if (bet.resultByMatchByUser != null) {
				bet.tmpResults = calc.getResults(bet.resultByMatchByUser);
			}
		}
	}
}
