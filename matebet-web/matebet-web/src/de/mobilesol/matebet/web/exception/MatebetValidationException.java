package de.mobilesol.matebet.web.exception;

public class MatebetValidationException extends MatebetException {

	private static final long serialVersionUID = 1L;

	public MatebetValidationException() {
		super();
	}

	public MatebetValidationException(int code, String message) {
		super(code, message);
	}
}
