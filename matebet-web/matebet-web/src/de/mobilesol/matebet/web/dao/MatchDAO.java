package de.mobilesol.matebet.web.dao;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.CompositeFilter;
import com.google.appengine.api.datastore.Query.CompositeFilterOperator;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import de.mobilesol.matebet.web.dto.MatchDTO;
import de.mobilesol.matebet.web.dto.MatchTipDTO;
import de.mobilesol.matebet.web.dto.MatchTipDTO.TipDTO;
import de.mobilesol.matebet.web.dto.MatchTipDTO.TipsByMatchByGroupMap;
import de.mobilesol.matebet.web.dto.MatchTipDTO.TipsByMatchMap;
import de.mobilesol.matebet.web.dto.TeamDTO;

public class MatchDAO {
	private static final Logger log = Logger
			.getLogger(MatchDAO.class.getName());
	private static final String TABLE = "match_t";

	public int addMatch(MatchDTO a) {
		log.info("addMatch" + a);

		DatastoreService datastore = DatastoreServiceFactory
				.getDatastoreService();

		Key key = KeyFactory.createKey(TABLE, a.matchId);
		try {
			datastore.get(key);
			throw new IllegalStateException(
					"id " + a.matchId + " already exists,");
		} catch (EntityNotFoundException e1) {
			Entity e = toEntity(new Entity(key), a);
			e.setProperty("created", new Date());

			datastore.put(e);

			return a.matchId;
		}
	}

	public void updateMatch(MatchDTO a) throws EntityNotFoundException {
		log.info("updateMatch" + a);

		DatastoreService datastore = DatastoreServiceFactory
				.getDatastoreService();

		Key key = KeyFactory.createKey(TABLE, a.matchId);
		Entity e = toEntity(datastore.get(key), a);
		e.setProperty("modified", new Date());
		datastore.put(e);
	}

	public MatchDTO getMatch(int id) {

		DatastoreService datastore = DatastoreServiceFactory
				.getDatastoreService();

		Query q = new Query(TABLE);
		q.setFilter(new FilterPredicate("matchId", FilterOperator.EQUAL, id));

		PreparedQuery pq = datastore.prepare(q);

		Iterator<Entity> it = pq.asIterator();
		if (it.hasNext()) {
			MatchDTO a = toObject(it.next());
			return a;
		}

		return null;
	}

	public List<MatchDTO> getMatchesByGroup(int groupId) {

		List<MatchDTO> matches = new ArrayList<MatchDTO>();
		MatchDTO m;

		m = new MatchDTO();
		m.matchId = 1;
		m.groupId = 1;
		m.team1 = new TeamDTO();
		m.team1.teamId = 1;
		m.team1.name = "decrease";
		m.matchdate = new Date();
		matches.add(m);
		
		m = new MatchDTO();
		m.matchId = 2;
		m.groupId = 1;
		m.team1 = new TeamDTO();
		m.team1.teamId = 2;
		m.team1.name = "increase";
		m.matchdate = new Date();
		matches.add(m);


		return matches;
	}

	public List<MatchDTO> getMatchesByLeague(int leagueId) {

		DatastoreService datastore = DatastoreServiceFactory
				.getDatastoreService();

		Query q = new Query(TABLE);
		q.setFilter(new FilterPredicate("leagueId", FilterOperator.EQUAL,
				leagueId));

		PreparedQuery pq = datastore.prepare(q);
		List<MatchDTO> l = new ArrayList<MatchDTO>();
		Iterator<Entity> it = pq.asIterator();
		while (it.hasNext()) {
			MatchDTO a = toObject(it.next());
			l.add(a);
		}

		return l;
	}

	public List<MatchDTO> deleteMatchsByLeague(int leagueId) {

		DatastoreService datastore = DatastoreServiceFactory
				.getDatastoreService();

		Query q = new Query(TABLE);
		q.setFilter(new FilterPredicate("leagueId", FilterOperator.EQUAL,
				leagueId));

		PreparedQuery pq = datastore.prepare(q);

		List<MatchDTO> list = new ArrayList<MatchDTO>();
		Iterator<Entity> it = pq.asIterator();
		while (it.hasNext()) {
			Entity e = it.next();
			MatchDTO b = toObject(e);
			datastore.delete(e.getKey());
			list.add(b);
		}

		return list;
	}

	public MatchDTO setMatchResult(MatchDTO result) {
		log.info("setMatchResult" + result);

		DatastoreService datastore = DatastoreServiceFactory
				.getDatastoreService();

		Query q = new Query(TABLE);
		q.setFilter(new FilterPredicate("matchId", FilterOperator.EQUAL,
				result.matchId));

		PreparedQuery pq = datastore.prepare(q);

		Iterator<Entity> it = pq.asIterator();
		if (it.hasNext()) {
			Entity e = it.next();
			Gson gson = new Gson();
			Map<String, Integer> m = new HashMap<String, Integer>();
			m.put("score1", result.score1);
			m.put("score2", result.score2);
			String text = gson.toJson(m);
			e.setProperty("result", text);
			e.setProperty("modified", new Date());
			datastore.put(e);

			return toObject(e);
		}

		return null;
	}

	public List<MatchDTO> getLatestMatches(int days) {

		List<MatchDTO> ret = new ArrayList<MatchDTO>();
		DatastoreService datastore = DatastoreServiceFactory
				.getDatastoreService();

		final long now = System.currentTimeMillis();

		Calendar cnow = new GregorianCalendar();
		cnow.add(Calendar.DAY_OF_YEAR, -days);

		log.info("lookup greater than " + cnow.getTime());
		Query q = new Query(TABLE);
		q.setFilter(new FilterPredicate("matchdate",
				FilterOperator.GREATER_THAN, cnow.getTimeInMillis()));
		q.addSort("matchdate");

		PreparedQuery pq = datastore.prepare(q);

		Iterator<Entity> it = pq.asIterator();
		while (it.hasNext()) {
			Entity e = it.next();
			Number mid = (Number) e.getProperty("matchId");
			Number mr = (Number) e.getProperty("matchdate");
			if (mr.longValue() + 1000 * 60 * 110 < now) {
				String result = (String) e.getProperty("result");
				// log.info("mid=" + mid + "; mr=" + mr + result);

				if (result != null) {
					// if result set, but no content
					Gson gson = new Gson();
					MatchDTO mrx = gson.fromJson(result, MatchDTO.class);
					if (mrx == null || mrx.score1 == null
							|| mrx.score2 == null) {
						result = null;
					}
				}

				if (result == null) {
					MatchDTO a = toObject(e);
					log.info("lookup match " + mid + ", " + a);
					ret.add(a);
				}
			} else {
				break;
			}
		}

		log.info("found " + ret.size() + " entries");

		return ret;
	}

	public List<MatchDTO> getMatchesBeginningSoon() {

		List<MatchDTO> ret = new ArrayList<MatchDTO>();
		handleMatches(ret, 15);
		// handleMatches(ret, 60 * 24 * 1);

		return ret;
	}

	private void handleMatches(List<MatchDTO> ret, int startIn) {
		DatastoreService datastore = DatastoreServiceFactory
				.getDatastoreService();

		Calendar min = new GregorianCalendar();
		min.add(Calendar.MINUTE, startIn);

		Calendar max = new GregorianCalendar();
		max.add(Calendar.MINUTE, startIn + 55);

		log.info("lookup greater than " + min.getTime());
		Query q = new Query(TABLE);
		q.setFilter(new FilterPredicate("matchdate",
				FilterOperator.GREATER_THAN, min.getTimeInMillis()));
		q.addSort("matchdate");

		PreparedQuery pq = datastore.prepare(q);

		Iterator<Entity> it = pq.asIterator();
		while (it.hasNext()) {
			Entity e = it.next();
			MatchDTO m = toObject(e);

			if (m.matchdate.before(max.getTime())) {
				log.info("found match " + m);
				ret.add(m);
			} else {
				break;
			}
		}

		log.info("found " + ret.size() + " entries");
	}

	public TipsByMatchByGroupMap getMatchTips(int leagueId) {

		DatastoreService datastore = DatastoreServiceFactory
				.getDatastoreService();

		Query q = new Query(TABLE);
		List<Filter> fa = new ArrayList<Filter>();
		fa.add(new FilterPredicate("leagueId", FilterOperator.EQUAL, leagueId));
		// if (groupId >= 0) {
		// fa.add(new FilterPredicate("groupId", FilterOperator.EQUAL,
		// groupId));
		// }
		if (fa.size() > 1) {
			q.setFilter(new CompositeFilter(CompositeFilterOperator.AND, fa));
		} else {
			q.setFilter(fa.get(0));
		}

		PreparedQuery pq = datastore.prepare(q);

		TipsByMatchByGroupMap ret = new TipsByMatchByGroupMap();

		ret.groups = new HashMap<Integer, MatchTipDTO.TipsByMatchMap>();

		Iterator<Entity> it = pq.asIterator();
		while (it.hasNext()) {
			Entity e = it.next();
			int gId = ((Number) e.getProperty("groupId")).intValue();
			int mId = ((Number) e.getProperty("matchId")).intValue();

			String result = (String) e.getProperty("result");
			Gson gson = new Gson();
			log.info("gId=" + gId + ", mId=" + mId + "; res=" + result);
			MatchDTO mr = null;
			if (result != null) {
				mr = gson.fromJson(result, MatchDTO.class);
				if (mr.score1 != null && mr.score2 != null) {
					TipsByMatchMap m = ret.groups.get(gId);
					if (m == null) {
						m = new TipsByMatchMap();
						m.tips = new HashMap<Integer, MatchTipDTO.TipDTO>();
						ret.groups.put(gId, m);
					}
					TipDTO t = m.tips.get(mId);
					if (t == null) {
						t = new TipDTO();
						m.tips.put(mId, t);
					}
					t.tipTeam1 = mr.score1;
					t.tipTeam2 = mr.score2;
				}
			}
		}

		log.info("ret=" + ret);
		return ret;
	}

	protected Entity toEntity(Entity e, MatchDTO a) {
		e.setProperty("matchId", a.matchId);
		e.setProperty("leagueId", a.leagueId);
		e.setProperty("season", a.season);
		e.setProperty("groupId", a.groupId);
		e.setProperty("groupOrderId", a.groupOrderId);

		e.setProperty("team1Id", a.team1.teamId);
		e.setProperty("team2Id", a.team2.teamId);
		e.setProperty("matchdate", a.matchdate.getTime());

		if (a.score1 != null && a.score2 != null) {
			Gson gson = new Gson();

			Map<String, Integer> m = new HashMap<String, Integer>();
			a.score1 = m.get("score1");
			a.score2 = m.get("score2");

			e.setProperty("result", gson.toJson(m));
		}

		return e;
	}

	protected MatchDTO toObject(Entity e) {
		MatchDTO a = new MatchDTO();
		a.matchId = ((Number) e.getProperty("matchId")).intValue();
		a.leagueId = ((Number) e.getProperty("leagueId")).intValue();
		a.season = (String) e.getProperty("season");
		a.groupId = ((Number) e.getProperty("groupId")).intValue();
		a.groupOrderId = ((Number) e.getProperty("groupOrderId")).intValue();

		a.team1.teamId = ((Number) e.getProperty("team1Id")).intValue();
		a.team2.teamId = ((Number) e.getProperty("team2Id")).intValue();

		a.matchdate = new Date(
				((Number) e.getProperty("matchdate")).longValue());

		String result = (String) e.getProperty("result");
		if (result != null) {
			Gson gson = new Gson();

			Type ct = new TypeToken<Map<String, Integer>>() {
			}.getType();

			Map<String, Integer> m = gson.fromJson(result, ct);
			a.score1 = m.get("score1");
			a.score2 = m.get("score2");
		}

		return a;
	}
}
