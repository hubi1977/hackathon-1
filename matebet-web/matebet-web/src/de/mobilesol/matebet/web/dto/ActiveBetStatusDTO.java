package de.mobilesol.matebet.web.dto;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class ActiveBetStatusDTO implements Serializable {

	private static final long serialVersionUID = 516056544918445787L;

	public Map<Integer, MatchTipStatusDTO> statusByGroup = new HashMap<Integer, MatchTipStatusDTO>();
	public String betStatus;

	public static class MatchTipStatusDTO implements Serializable {

		private static final long serialVersionUID = -6477894564434192023L;

		public Map<Integer, BetMatchStatus> statusByMatch;
		public BetGroupStatus groupStatus;

		public MatchTipStatusDTO() {
		}

		@Override
		public String toString() {
			return "MatchTipStatusDTO [statusByMatch=" + statusByMatch
					+ ", groupStatus=" + groupStatus + "]";
		}
	}

	public static enum BetGroupStatus {
		INITIATED(0), YOUR_MOVE(1), OTHER_MOVE(2), PENDING(3), CLOSED(
				4), FINISHED(5);
		public int value;

		private BetGroupStatus(int value) {
			this.value = value;
		}
	};

	public static enum BetMatchStatus {
		EDIT_USER(0), EDIT_BOTH(1), SHOW_BOTH(2), SHOW_USER(3), SHOW_NONE(
				4), FINISHED(6);
		public int value;

		private BetMatchStatus(int value) {
			this.value = value;
		}
	}

	@Override
	public String toString() {
		return "ActiveBetStatusDTO [statusByGroup=" + statusByGroup
				+ ", betStatus=" + betStatus + "]";
	};
}
