package de.mobilesol.matebet.web.dto;

import java.io.Serializable;

public class TeamDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	public int teamId;
	public String name;
	public String url;

	@Override
	public String toString() {
		return name + " (" + teamId + ")";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + teamId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TeamDTO other = (TeamDTO) obj;
		if (teamId != other.teamId)
			return false;
		return true;
	}

}
