package de.mobilesol.matebet.web.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.cache.Cache;
import javax.cache.CacheException;
import javax.cache.CacheFactory;
import javax.cache.CacheManager;

import org.apache.log4j.Logger;

import de.mobilesol.matebet.web.dto.ActiveBetStatusDTO;
import de.mobilesol.matebet.web.dto.ActiveBetStatusDTO.BetGroupStatus;
import de.mobilesol.matebet.web.dto.ActiveBetStatusDTO.BetMatchStatus;
import de.mobilesol.matebet.web.dto.ActiveBetStatusDTO.MatchTipStatusDTO;
import de.mobilesol.matebet.web.dto.BetDTO;
import de.mobilesol.matebet.web.dto.BetDTO.BetStatus;
import de.mobilesol.matebet.web.dto.BetGroupDTO;
import de.mobilesol.matebet.web.dto.BetMatchDTO;
import de.mobilesol.matebet.web.dto.MatchDTO;
import de.mobilesol.matebet.web.dto.MatchTipDTO;
import de.mobilesol.matebet.web.dto.MatchTipDTO.TipsByMatchMap;
import de.mobilesol.matebet.web.dto.TeamDTO;

public class Util {
	private static final Logger log = Logger.getLogger(Util.class.getName());

	public static ActiveBetStatusDTO determineActiveStatus(BetDTO bet,
			MatchTipDTO ut, int userId) {

		ActiveBetStatusDTO activeStatus = new ActiveBetStatusDTO();

		List<BetGroupStatus> previousStatus = new ArrayList<BetGroupStatus>();

		//
		// set group status
		//
		for (BetGroupDTO g : bet.groups) {

			log.info("handle group " + g);
			MatchTipStatusDTO ret = Util.handleGroup(g, ut, userId,
					previousStatus, bet);

			activeStatus.statusByGroup.put(g.groupId, ret);
			previousStatus.add(ret.groupStatus);
		}

		//
		// set bet status
		//
		if (bet.status == BetStatus.RUNNING) {
			activeStatus.betStatus = "OTHER_MOVE";

			for (BetGroupDTO group : bet.groups) {
				log.info("handle group " + group);

				if (activeStatus.statusByGroup.get(
						group.groupId).groupStatus == BetGroupStatus.YOUR_MOVE) {
					activeStatus.betStatus = "YOUR_MOVE";
				}
			}
		} else if (bet.status == BetStatus.INITIATED) {
			if (bet.userIds.get(0) != userId) {
				activeStatus.betStatus = "WAITING_FOR_CONFIRM";
			} else {
				activeStatus.betStatus = "WAITING_FOR_OTHER_TO_CONFIRM";
			}
		} else if (bet.status == BetStatus.CLOSED) {
			activeStatus.betStatus = "CLOSED";
		}

		return activeStatus;
	}

	private static MatchTipStatusDTO handleGroup(final BetGroupDTO group,
			final MatchTipDTO ut, final int userId,
			List<BetGroupStatus> previousStatus, BetDTO bet) {

		long currentDate = System.currentTimeMillis();
		boolean isUserOwner = (bet.userIds.get(0) == userId);

		// TODO: FIXME, works only for two users
		int opponentId = bet.userIds.get(1);
		if (opponentId == userId) {
			opponentId = bet.userIds.get(0);
		}

		final MatchTipStatusDTO ret = new MatchTipStatusDTO();
		Map<Integer, MatchTipDTO.TipsByMatchMap> matchTipsByUser = new HashMap<Integer, MatchTipDTO.TipsByMatchMap>();

		for (int u : bet.userIds) {
			// User match Tips
			if (ut.groupsByUser.get(userId) != null) {
				TipsByMatchMap tips = ut.groupsByUser.get(u) != null
						? ut.groupsByUser.get(u).groups.get(group.groupId)
						: null;
				if (tips == null) {
					tips = new TipsByMatchMap();
					tips.tips = new HashMap<Integer, MatchTipDTO.TipDTO>();
				}
				matchTipsByUser.put(u, tips);
			}
		}

		if (group.matches == null) {
			log.warn("group does not have matches");
			group.matches = Collections.emptyList();
		}

		//
		// determine Match Status
		//
		ret.statusByMatch = Util.determineMatchStatus(group.matches,
				currentDate, bet, userId, matchTipsByUser, bet.status);

		//
		// determine group status
		//
		ret.groupStatus = Util.determineGroupStatus(ret.statusByMatch.values(),
				group.lastMatchdate.getTime(), previousStatus, currentDate,
				isUserOwner, bet.status);
		if (ret.groupStatus == BetGroupStatus.PENDING) {
			for (Map.Entry<Integer, BetMatchStatus> entry : ret.statusByMatch
					.entrySet()) {
				entry.setValue(BetMatchStatus.SHOW_NONE);
			}
		}

		return ret;
	}

	private static Map<Integer, BetMatchStatus> determineMatchStatus(
			List<BetMatchDTO> matches, long currentDate, BetDTO bet, int userId,
			Map<Integer, MatchTipDTO.TipsByMatchMap> matchTipsByUser,
			BetStatus betStatus) {

		Map<Integer, BetMatchStatus> statusByMatch = new HashMap<Integer, BetMatchStatus>();

		for (BetMatchDTO m : matches) {
			log.info("handle match " + m);

			boolean userTipped = matchTipsByUser.get(userId) != null
					&& matchTipsByUser.get(userId).tips.containsKey(m.matchId);
			boolean allTipped = true;
			for (int u : bet.userIds) {
				if (u != userId && (matchTipsByUser.get(u) == null
						|| !matchTipsByUser.get(u).tips
								.containsKey(m.matchId))) {
					allTipped = false;
				}
			}

			if (currentDate > m.matchdate.getTime()) {
				statusByMatch.put(m.matchId, BetMatchStatus.FINISHED);
			} else if (betStatus == BetStatus.INITIATED) {
				statusByMatch.put(m.matchId, BetMatchStatus.SHOW_NONE);
			} else if (userTipped && allTipped) {
				statusByMatch.put(m.matchId, BetMatchStatus.SHOW_BOTH);
			} else if (userTipped) {
				statusByMatch.put(m.matchId, BetMatchStatus.SHOW_USER);
			} else {
				statusByMatch.put(m.matchId, BetMatchStatus.EDIT_USER);
			}
		}
		log.info("match stati " + statusByMatch);

		return statusByMatch;
	}

	private static BetGroupStatus determineGroupStatus(
			Collection<BetMatchStatus> matchStati, long latestMatchInGroup,
			List<BetGroupStatus> previousStatus, long currentDate,
			boolean isUserOwner, BetStatus betStatus) {

		boolean edit = false;
		boolean other = false;
		boolean noTipAtAll = true;
		for (BetMatchStatus st : matchStati) {
			switch (st) {
				case EDIT_USER : {
					edit = true;
					noTipAtAll = false;
					break;
				}
				case EDIT_BOTH : {
					edit = true;
					break;
				}
				case SHOW_USER : {
					other = true;
					noTipAtAll = false;
					break;
				}
				case SHOW_BOTH : {
					noTipAtAll = false;
					break;
				}
			}
		}

		BetGroupStatus groupStatus = null;
		if (latestMatchInGroup < currentDate) {
			groupStatus = BetGroupStatus.FINISHED;
		} else if (betStatus == BetStatus.INITIATED) {
			groupStatus = BetGroupStatus.INITIATED;
		} else if (noTipAtAll) {
			if (previousStatus.size() > 0
					&& previousStatus.get(previousStatus.size()
							- 1) != BetGroupStatus.FINISHED
					&& previousStatus.get(previousStatus.size()
							- 1) != BetGroupStatus.CLOSED) {
				// if status is initiated and previous status is not
				// closed,
				// then set to pending
				groupStatus = BetGroupStatus.PENDING;
			} else if (previousStatus.size() > 0) {
				groupStatus = BetGroupStatus.YOUR_MOVE;
				// } else if (previousStatus.size() == 0 && isUserOwner) {
				// groupStatus = BetGroupStatus.OTHER_MOVE;
			} else if (previousStatus.size() == 0) {
				groupStatus = BetGroupStatus.YOUR_MOVE;
			}
		} else if (edit) {
			groupStatus = BetGroupStatus.YOUR_MOVE;
		} else if (other) {
			groupStatus = BetGroupStatus.OTHER_MOVE;
		} else {
			groupStatus = BetGroupStatus.CLOSED;
		}

		return groupStatus;
	}

	public static void addTeamUrls(BetDTO b) {
		if (b == null || b.groups == null) {
			log.warn("bet " + b + " does not have groups");
		}
		for (BetGroupDTO g : b.groups) {
			for (BetMatchDTO m : g.matches) {
				addTeamUrls(m.team1);
				addTeamUrls(m.team2);
			}
		}
	}

	public static void addTeamUrls(MatchDTO m) {
		addTeamUrls(m.team1);
		addTeamUrls(m.team2);
	}

	public static void addTeamUrls(TeamDTO m) {
		m.url = "http://api.matebet.net/images/icon-" + m.teamId + ".svg";
	}

	public static Cache getCache() {
		Cache cache;
		try {
			CacheFactory cacheFactory = CacheManager.getInstance()
					.getCacheFactory();
			cache = cacheFactory.createCache(Collections.emptyMap());
			return cache;
		} catch (CacheException e) {
			log.warn("failed", e);
			return null;
		}
	}
}
