package de.mobilesol.matebet.web.util;

import java.util.Comparator;

import de.mobilesol.matebet.web.dto.MatchDTO;

public class ComparatorUtil {

	public static final Comparator<MatchDTO> MATCH_COMPARATOR = new Comparator<MatchDTO>() {

		@Override
		public int compare(MatchDTO o1, MatchDTO o2) {
			long diff = o1.matchdate.getTime() - o2.matchdate.getTime();
			if (diff < 0) {
				return -1;
			} else if (diff > 0) {
				return 1;
			}

			return o1.matchId - o2.matchId;
		}
	};
}
