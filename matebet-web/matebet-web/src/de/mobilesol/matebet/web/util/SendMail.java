package de.mobilesol.matebet.web.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

import org.apache.log4j.Logger;

public class SendMail {

	private static final Logger log = Logger
			.getLogger(SendMail.class.getName());

	public String send(String subject, String text, String recipientName,
			String recipientEmail, String replyTo) {
		try {
			log.info("send mail to " + recipientEmail + "(" + recipientName
					+ ")");
			String ret = send(subject, text,
					new InternetAddress(recipientEmail, recipientName),
					replyTo);

			return ret;
		} catch (Throwable e) {
			log.error("hupps", e);
			return e.getMessage();
		}
	}

	private String send(String subject, String text, InternetAddress recipient,
			String replyTo) throws MessagingException, IOException {
		Properties props = new Properties();
		Session session = Session.getDefaultInstance(props, null);

		MimeMessage msg = new MimeMessage(session);
		msg.setFrom(new InternetAddress("noreply@matebet-1222.appspotmail.com",
				"mateBet <noreply@matebet.net>"));
		if (replyTo != null) {
			msg.setReplyTo(
					new Address[]{new InternetAddress(replyTo, replyTo)});
		} else {
			msg.setReplyTo(new Address[]{
					new InternetAddress("noreply@matebet.net", "mateBet")});
		}
		// InternetAddress("lui.baeumer@googlemail.com", "Lui")});
		// msg.addRecipient(Message.RecipientType.TO,
		// new InternetAddress("lui.baeumer@googlemail.com", "LuiXu"));
		msg.addRecipient(Message.RecipientType.TO, recipient);
		// msg.addRecipient(Message.RecipientType.BCC, new InternetAddress(
		// "lui.baeumer@gmail.com, "Lui"));
		msg.setSubject(subject, "UTF-8");

		// image
		MimeMultipart multipart = new MimeMultipart("related");

		BodyPart messageBodyPart = new MimeBodyPart();

		StringBuffer strb = new StringBuffer();
		strb.append(
				"<div style=\"border: 10px solid #438eff; margin: 0px; padding: 0px; font-family:'Helvetica Neue',Helvetica,sans-serif;\" >");
		strb.append("  <div style=\"width: 100%; background: #05026d; \">");
		strb.append(
				"    <img style=\"max-width: 150px; height: auto;\" src=\"cid:image.png\"/>");
		strb.append("  </div>");

		strb.append(
				"<div style=\"padding-left: 5px; padding-top: 10px; padding-bottom:10px; padding-right:5px; \">");
		strb.append(text);
		strb.append("</div></div>");

		messageBodyPart.setContent(strb.toString(), "text/html; charset=utf-8");

		multipart.addBodyPart(messageBodyPart);

		// 2.step image
		MimeBodyPart imagePart = new MimeBodyPart();
		InputStream in = this.getClass().getResourceAsStream("/email.png");
		DataSource fds = new ByteArrayDataSource(in, "image/png");
		log.info("fds=" + fds.getInputStream() + ";" + fds.getName());
		imagePart.setDataHandler(new DataHandler(fds));
		imagePart.setHeader("Content-ID", "<image.png>");
		imagePart.setDisposition(Part.INLINE);
		imagePart.setFileName("image.png");

		// add image to the multipart
		multipart.addBodyPart(imagePart);

		msg.setContent(multipart);

		Transport.send(msg);

		log.info("Send mail:" + strb);

		return strb.toString();
	}
}
